- [x] ~~update CSVParser to accept configuration rather than only understanding a
      hard-coded CSV type~~
- [x] ~~allow user to specify in config file how to parse~~
- [ ] Update csv-processor to append year to CC-Transaction-Date
- [ ] add comma to amount
- [x] ~~Add support for non-split input column~~
- [ ] update cli flags to allow specifying config file location
- [x] ~~allow to specify case sensitivity in parser components.~~
- [ ] allow to specify whether or not to check for spaces between components
- [ ] Gracefully allow for more than one parser to match - maybe notify user in a
      ledger tag with the parser name?
- [ ] Add option to put matching parser name in the ledger file.
- [ ] Implement optional space after Description chunk
- [x] ~~notify user if no Description component is provided (oh wait, this is taken
      care of already, right?...no it's not.)~~
- [x] ~~add something that allows to capture "all remaining", or a setting that by
      default takes any unprocessed text and dump it in a tag~~
- [ ] allow user to specify date type
- [ ] let user determine whether to add a "-" to a split debit/credit column or
      not
- [ ] add more error-detection to CSV import, i.e. can we turn an amount into a
      float?
- [ ] allow user to select whether to clear ("*") transactions or not
- [ ] allow user to select how much to indent, and whether to use tabs or spaces
- [ ] figure out if we really need to export `MyData(..)` so many different
      times. This feels like "code smell"
- [ ] Update tag processing such that empty data does not propogate
