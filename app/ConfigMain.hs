module Main (main) where

import Control.Exception (Exception, displayException)
import ConfigParser2
import Config(parse)
import Config.Schema (loadValue, generateDocs)
import qualified Data.Text.IO as TextIO

main :: IO ()
main = do
    putStrLn "Hello, jali!"

    -- Parse configuration file
    rawConf <- TextIO.readFile "config2.cfgv"
    case loadValue infileSpec rawConf of
        Left err -> putStrLn (displayException err)
        Right val -> putStrLn (show val)
    print (generateDocs infileSpec)

loadConf :: Exception e => Text -> Either e InputFile
loadConf inData = runExceptT $ do
    parsedFile <- exceptT (parse inData)
    exceptT (loadVale infileSpec parsedFile
