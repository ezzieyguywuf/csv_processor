{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
module Main (main) where

-- Local imports
import Parsers.CommandLine (FilePathT(..), argParser)
import Parsers.ConfigFile( topSpec)
import Parsers.CSV (Transaction(..), processCSV)
import Parsers.Description (processDescription)
import Data.Tag (Tag(..))
import Data.Configurations ( ParserConf, getParserName, changeFilePath
                           , confInfileData , confParsers
                           )

-- Prelude imports
import System.Exit (die, exitFailure)
import qualified Data.List.NonEmpty as NE
import Control.Exception (IOException, handle, displayException)
import Data.Text (Text, unpack)
import qualified Data.Text as Text
import qualified Data.Text.IO as TextIO

-- Third-party imports
import Config (parse)
import Config.Schema (ValueSpec, generateDocs, loadValue)

main :: IO ()
main = do
    -- Parse the configuration file
    conf <- getConfigInfoOrDie topSpec "./config3.cfgv"
    -- Parse command-line
    cliArgs <- argParser

    -- determine which input file to use
    let inFileData' = confInfileData conf
        inFileData  = maybe inFileData' (changeFilePath inFileData') (getFilePath cliArgs)
        -- Get our list of parsers
        parsers     = confParsers conf

    putStrLn "Found parsers: "
    mapM_ (putStrLn . ("    " <>) . show . getParserName) parsers

    processCSV inFileData >>=
        \case
            -- Error processing the CSV
            Left err -> die (unpack err)
            -- Process the "raw" descriptions, also maybe adding more tags
            Right transactions -> do
                let transactions' = fmap (processDescription parsers) transactions
                mapM_ (makeHledgerHeader parsers) transactions'
                -- TextIO.writeFile "processd.ledger" (Text.unlines outlines)
                -- putStrLn "Wrote file to processd.ledger"

-- | Turns a Transaction into an hledger transaction "header", i.e. the
--   top-matter
makeHledgerHeader :: NE.NonEmpty ParserConf -> Transaction -> IO ()
makeHledgerHeader parsers transaction = do
    let (Transaction date desc _ tags) = processDescription parsers transaction
    -- Prints the first line, including the processed Description
    TextIO.putStrLn (Text.pack (show date) <> " * " <> Text.pack (show desc))
    -- Each tag will go on its own line
    mapM_ makeHledgerTag tags

-- | Makes a hledger-style tag
makeHledgerTag :: Tag -> IO ()
makeHledgerTag (Tag name tagdata) =
    if Text.null tagdata
        -- Don't do anything if the data is empty.
        then pure ()
        else TextIO.putStrLn (Text.pack "    ; " <> name <> Text.pack ": " <> tagdata)

-- | Tries to open the file, Config.parse it, then Config.Schema.loadValue the
--   parsed file. Any error results in the program exiting with a relevant error
--   message
getConfigInfoOrDie :: ValueSpec a -> FilePath -> IO a
getConfigInfoOrDie vSpec fpath =
    getFileData fpath >>= \case
        Left err -> die err -- IOError, probs file doesn't exist
        Right fData -> case parse fData of
            Left err -> die (displayException err) -- parse error in file
            Right vData -> case loadValue vSpec vData of
                Left err -> do putStrLn (displayException err)
                               print (generateDocs vSpec)
                               exitFailure
                Right vals -> pure vals

-- | Either opens the file or prints out an error
getFileData :: FilePath -> IO (Either String Text)
getFileData fpath = handle (pure . Left . (displayException :: IOException -> String))
                           (Right <$> TextIO.readFile fpath)
