module Parsers.CommandLine
( FilePathT(..)
, argParser
) where

-- Prelude imports
import Data.Text (Text)

-- Third-party imports
import Options.Applicative

-- | The various command-line arguments we understand
data FilePathT = FilePathT {getFilePath :: Maybe Text} deriving (Show)

-- | The final option parser to pass to an optparse-applicative parser, i.e.
-- execParser
argParser :: IO FilePathT
argParser = execParser myParser
    where myParser = info (targetFile <**> helper) programInfo

-- | The program information needed to describe the final parser
programInfo :: InfoMod FilePathT
programInfo = fullDesc <> progDesc programDescription <> header programHeader

programDescription :: String
programDescription = "This program can be used to pre-process a CSV file for \
\use in an hledger importer. Specifically, it will read the description and \
\parse it, adding new columns. (I know, this description sucks.)"

programHeader :: String
programHeader = "csv-processer - a processor of CSV files"

-- | Target file provided from cli
targetFile :: Parser FilePathT
targetFile = FilePathT <$>
    option auto
    (  long "target-file"
    <> short 'f'
    <> metavar "INFILE"
    <> value Nothing
    <> help "The target file to process. Overrides configuration file value."
    )
