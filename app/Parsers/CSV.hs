{-# LANGUAGE OverloadedStrings #-}
module Parsers.CSV
( Transaction(..)
, processCSV
)
where

-- Prelude imports
import qualified Data.ByteString.Lazy as B
import qualified Data.Vector as V
import Data.Either (partitionEithers)

-- Third-party imports
import Data.Text (Text, pack, unpack)
import qualified Data.Text as Text
import Data.Csv (HasHeader(HasHeader), decode)
import Data.Time (Day, parseTimeM, defaultTimeLocale, formatTime)

-- Local Imports
import Data.Transaction (Transaction(..), makeTransaction)
import Data.Configurations ( InputFile, getInputFilePath)

-- | Given the "InputFile" data, either returns an error or the parsed data
processCSV :: InputFile -> IO (Either Text [Transaction])
processCSV inFileData = do
    csvData <- B.readFile (Text.unpack $ getInputFilePath inFileData)
    pure $ do
        -- "decode" returns a `type Csv = [[ByteString]]`
        csvData' <- either (Left . pack) Right (decode HasHeader csvData)
        let mapFunc = makeTransaction inFileData
            (errs, mapped) = partitionEithers (fmap mapFunc (V.toList csvData'))

        -- Any error and we'll bail out.
        if not (null errs)
            then Left (head errs)
            else Right mapped

-- | Coverts the date from the input format to the hledger format
_convertDate :: Text -> Text
_convertDate inDate = pack outDate
    where minDate' = parseTimeM True defaultTimeLocale "%-m/%-d/%Y" (unpack inDate) :: Maybe Day
          outDate = case minDate' of
                        Nothing -> ""
                        Just inDate' -> formatTime defaultTimeLocale "%Y-%m-%d" inDate'
