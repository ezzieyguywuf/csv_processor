{-# LANGUAGE OverloadedStrings #-}
module Parsers.Description
( ProcessedText
, processDescription
)
where

-- Base imports
import qualified Data.List.NonEmpty as NE
import Data.Maybe (catMaybes)
import Data.Either (rights)

-- Local imports
import Parsers.ConfigFile (ParserConf(..), NamedGroup(..))

-- Third-Party Imports
import qualified Data.Text as Text
import Data.Text (Text, pack, unpack)
import Data.Transaction (Transaction(..))
import Data.Tag (Tag(..))
import Text.Regex.PCRE ((=~))

-- | The final result of parsing
data ProcessedText = ProcessedText { _getDescription :: Text
                                   , _getTags :: [Tag]} deriving (Show)

-- | Tries to process the text using the given parsers, or else returns it
--   un-processed. Only returns the results of the first parser to match.
processDescription :: NE.NonEmpty ParserConf -> Transaction -> Transaction
processDescription parsers transaction@(Transaction date desc amt tags) =
    case rights (NE.toList parsed) of
        []    -> transaction
        ((ProcessedText desc' tags'):_) -> Transaction date desc' amt (tags <> tags')
    where parsed = fmap (tryRegex desc) parsers

-- | Note that the index supplied by ParserConf is 1-based, but haskell lists
--   are 0-based, so this must be taken into account
tryRegex :: Text -> ParserConf -> Either Text ProcessedText
tryRegex inText (ParserConf _ pattern n namedgroups) = do
    let (_, match, _, groups) = (unpack inText) =~ (unpack pattern) :: (String, String, String, [String])
        groups' = fmap pack groups
        n'      = n - 1
    checkMatch (pack match)
    checkIndex groups n
    let description = pack (groups !! n')
        tags = catMaybes (fmap (checkGroup groups') namedgroups)
    Right (ProcessedText description tags)

-- | Helper to return Left with a message if there was no match
checkMatch :: Text -> Either Text ()
checkMatch inText
    | Text.null inText == True = Left "Empty match"
    | otherwise                = Right ()

-- | Helper to return a message if the provided index is out-of-range
checkIndex :: [String] -> Int -> Either Text ()
checkIndex groups n
    | n - 1 >= length groups = Left $ "The index is out-of-range for the match-groups: "
                                    <> (pack . show $ (length groups))
                                    <> " matches found, but index provided is " 
                                    <> (pack . show $ n)
    | n - 1 < 0              = Left "The index must be 1 or greater"
    | otherwise              = Right ()

-- | Again, we have to take care of 1-based regex groups vs 0-based haskell
--   lists
checkGroup :: [Text] -> NamedGroup -> Maybe Tag
checkGroup groups (NamedGroup name n)
    | n - 1 >= length groups = Nothing
    | n - 1 < 0              = Nothing
    | otherwise              = Just (Tag name (groups !! (n - 1)))
