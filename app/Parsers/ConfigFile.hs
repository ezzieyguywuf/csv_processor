{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE LambdaCase #-}
module Parsers.ConfigFile
( 
  TopConf
, ParserConf(..)
, NamedGroup(..)
, InputFile(..)
, InputFileColumns(..)
, AmountColumn(..)
, TagColumn(..)
, FileColumn
, topSpec
)
where

-- Prelude imports
import qualified Data.List.NonEmpty as NE

-- Third-party imports
import Config.Schema
import Data.Text (Text)

-- Local imports
import Data.Configurations ( TopConf (TopConf), InputFile (InputFile)
                           , ParserConf (ParserConf)
                           , DescriptionProcessor (DescriptionProcessor)
                           , InputFile(..) , GlobalConf(GlobalConf)
                           , InputFileColumns (InputFileColumns)
                           , TagColumn (TagColumn), FileColumn (FileColumn)
                           , AmountColumn (AmountColumn, SplitAmount)
                           , NamedGroup (NamedGroup)
                           , DescriptionMatch (DescriptionMatch))

--   --------------------------------------------------------------------------
-- * These are the specifications for the various components in our
--   configuration file
--   --------------------------------------------------------------------------

-- | The top-level specification for the configuration file
topSpec :: ValueSpec TopConf
topSpec = sectionsSpec "" $ do
    globSpec <- reqSection' "Global" globalSpec 
                            "The top-level configuration options"
    parseSpec <- reqSection' "Parsers" (oneOrList parserSpec)
                             "The list of parsers throw at incoming descriptions"
    descProcs <- reqSection' "DescriptionMatchers" (oneOrList procSpec)
                             "These matchers can be used to process \
                             \descriptions into transactions"
    pure (TopConf globSpec (NE.fromList parseSpec) (NE.fromList descProcs))

-- | The global settings
globalSpec :: ValueSpec GlobalConf
globalSpec = sectionsSpec "" $ do
    inFile <- reqSection' "InputFile" infileSpec
                          "Data needed to process the input file"
    outFile <- reqSection "OutputFile" "Where to write the processed data"
    pure (GlobalConf inFile outFile)


-- | This spec describes the information needed for an input file
infileSpec :: ValueSpec InputFile
infileSpec = sectionsSpec "00-Input File Info" $ do
    fpath <- reqSection "Path" "The PATH to the input file."
    (columns, tags) <- reqSection' "Columns" columnsSpec
                           "Describes the various columns in the input file"
    pure (InputFile fpath columns tags)

-- | The columns in the input file
columnsSpec :: ValueSpec (InputFileColumns, [TagColumn])
columnsSpec = sectionsSpec "01-Input file column" $ do
    dateCol <- indexSpec "Date"
    descCol <- indexSpec "Description"
    amtCol  <- reqSection' "Amount" amtSpec ""
    extraCol <- optSection' "Extra" (oneOrList tagSpec)
                            "(optional) Columns to add as tags"

    pure (InputFileColumns dateCol descCol amtCol, concat extraCol)

-- | An index to a column in the input file
indexSpec :: Text -> SectionsSpec FileColumn
indexSpec desc = reqSection desc "A zero-based index"

instance HasSpec FileColumn where
    anySpec = FileColumn <$> integerSpec

-- | Specifies an AmountColumn
amtSpec :: ValueSpec AmountColumn
amtSpec = sectionsSpec "02-Amount Column" $ do
    crd <- reqSection "Credit" "The credit column, zero-based index"
    dbt <- reqSection "Debit" "The debit column, zero-based index"
    pure (SplitAmount crd dbt)

-- | Spocifies  a Tag
tagSpec :: ValueSpec TagColumn
tagSpec = sectionsSpec "03-Tag Column" $ do
    nme <- reqSection "Name" "The name to use for the tag in the ledger file"
    col <- reqSection "Column" "The tag column, zero-based index"
    pure (TagColumn nme col)

-- | Specifies a Description string parser
parserSpec :: ValueSpec ParserConf
parserSpec = sectionsSpec "Parser" $ do
    name    <- reqSection "Name" "A name for the described parser"
    pattern <- reqSection "Pattern"
                           "A Regular Expression used to try to match a description"
    descr   <- reqSection "Description" "Which matched group should be used as the description"
    names   <- concat <$> optSection' "NamedGroups" (oneOrList namedGroupSpec)
                                      "A list of NamedGroup"
    pure (ParserConf name pattern descr names)

-- | Specifies a named group from the regex match
namedGroupSpec :: ValueSpec NamedGroup
namedGroupSpec = sectionsSpec "NamedGroup" $ do
    name <- reqSection "Name" "The name to give the capture group"
    n    <- reqSection "Index" "Which capture group to give this name"
    pure (NamedGroup name n)

-- | An entire description "processor", which can create a ledger posting
procSpec :: ValueSpec DescriptionProcessor
procSpec = sectionsSpec "One-Processor" $ do
    matchers <- reqSection' "Matches"
                            (oneOrList matchSpec)
                            "The list of matchers to use for incoming descriptions"
    dbtAct <- reqSection "DebitAccount" "The account to debit from"
    crdAct <- reqSection "CreditAccount" "The account to credit to"
    pure (DescriptionProcessor matchers dbtAct crdAct)

-- | A single match specification
matchSpec :: ValueSpec DescriptionMatch
matchSpec = sectionsSpec "Description-Match" $ do
    newDescr <- reqSection "NewDescription"
                           "Any succesful match will be turned into this string"
    mtchStrs <- reqSection' "MatchStrings" (oneOrList textSpec)
                           "A list of one or more strings to match against\
                           \ a description"
    -- `concat (Maybe [a])` produces an empty list or a list: handy!
    pure (DescriptionMatch (NE.fromList mtchStrs) newDescr)
