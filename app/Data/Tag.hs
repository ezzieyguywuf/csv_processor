module Data.Tag (Tag(..)) where

-- Third-party imports
import Data.Text (Text)

data Tag = Tag {getTagName :: Text, getTagData :: Text} deriving (Show)
