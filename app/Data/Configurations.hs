module Data.Configurations 
(
-- * Data Types
  TopConf (TopConf)
, InputFile (InputFile)
, ParserConf (ParserConf)
, DescriptionProcessor (DescriptionProcessor)
, GlobalConf (GlobalConf)
, InputFileColumns (InputFileColumns)
, TagColumn (TagColumn)
, FileColumn (FileColumn)
, AmountColumn (AmountColumn, SplitAmount)
, NamedGroup (NamedGroup)
, DescriptionMatch (DescriptionMatch)
-- * Accesors for the datatypes exported
, confInfileData
, confParsers
, confDescriptionProcessors
, getInputFilePath
, getParserName
, getColumnIndex
, getInputColumns
, getInputTags
, getDateColumn
, getDescriptionColumn
, getAmountColumn
-- * Helper functions to change data in exported datatypes
, makeProcessedDescription
, changeFilePath
)
where

-- local imports
import Data.Tag

-- Prelude imports
import qualified Data.List.NonEmpty as NE

-- Third-party imports
import Data.Text (Text)

-- | Creates a processed description, returning the first succesful match from
--   the list of DescriptionProcessor
makeProcessedDescription :: NE.NonEmpty DescriptionProcessor
                         -> Description
                         -> ProcessedDescription
makeProcessedDescription _ _ = undefined

-- | Returns the input file PATH from the TopConf
confInfileData :: TopConf -> InputFile
confInfileData = _getInputFile . _getGlobalConf

-- | Returns the list of parser configurations that were found
confParsers :: TopConf -> NE.NonEmpty ParserConf
confParsers = _getParsers

-- | Returns the list of description processors that were found
confDescriptionProcessors :: TopConf -> NE.NonEmpty DescriptionProcessor
confDescriptionProcessors = _getDescProcesors

-- | Changes the FilePath of the given InputFile
changeFilePath :: InputFile -> Text -> InputFile
changeFilePath (InputFile _ cols tags) fpath = InputFile fpath cols tags


--   --------------------------------------------------------------------------
-- * The data types that are used
--   --------------------------------------------------------------------------

-- | Describes the Top-Level Configuration
data TopConf = TopConf { _getGlobalConf    :: GlobalConf
                       , _getParsers       :: NE.NonEmpty ParserConf
                       , _getDescProcesors :: NE.NonEmpty DescriptionProcessor
                       } deriving (Show)

-- | Describes the global configs
data GlobalConf =GlobalConf { _getInputFile  :: InputFile
                            , _getOutputFile :: Text}
                            deriving (Show)

-- | Provides the information needed to make a Hledger transaction description
--   and the postings
data ProcessedDescription = ProcessedDescription
    { description :: Text
    , accounts    :: (Account, Account)
    }
    deriving (Show)

newtype Account = Account {getAccountName :: Text} deriving (Show)

-- | The result of parsing a description text using regex
data Description = Description { _getDescription :: Text
                               , _getTags :: [Tag]} deriving (Show)


-- | Describes a parser to use on a Description string
--
--   Note: Regex match-groups are 1-based, not 0-based. We store the
--   user-supplied index as-is, so it will be 1-based
data ParserConf = ParserConf { getParserName       :: Text
                             , getRegexPattern     :: Text
                             , getDescriptionGroup :: Int
                             , getGroupNames       :: [NamedGroup]
                             } deriving (Show)

-- | A Named Group is a matched regex "group" that we give a name
data NamedGroup = NamedGroup { getGroupName  :: Text
                             , getGroupIndex :: Int
                             } deriving (Show)

-- | Describes an input file, and its associated columns
data InputFile = InputFile
    { getInputFilePath :: Text
    , getInputColumns  :: InputFileColumns
    , getInputTags     :: [TagColumn]
    } deriving (Show)

-- | Describes a zero-indexed column number in an input file
newtype FileColumn = FileColumn {getColumnIndex :: Integer} deriving (Show)

-- | These are some required columns from the input file
data InputFileColumns = InputFileColumns
    { getDateColumn        :: FileColumn
    , getDescriptionColumn :: FileColumn
    , getAmountColumn      :: AmountColumn
    } deriving (Show)

-- | The user can specify any arbitrary column to be stored as a tag
data TagColumn = TagColumn { getTagName :: Text
                           , getTagCol :: FileColumn}
                           deriving (Show)

-- | Allows for split debit/credit columns
data AmountColumn = AmountColumn FileColumn
                  | SplitAmount { getColDebit  :: FileColumn
                                , getColCredit :: FileColumn
                                } deriving (Show)

-- | Describes the information needed to process a description. Here, "process"
--   means
--
--   1. Match a description
--   2. "Mux" the description into a "pretty" form
--   3. Create a ledger posting with it
data DescriptionProcessor = DescriptionProcessor
    { matchers      :: [DescriptionMatch]
    , debitAccount  :: Text
    , creditAccount :: Text
    } deriving (Show)

-- | This describes a list of matches, any of which will be changed to th new
--   description
data DescriptionMatch = DescriptionMatch
    { matches  :: NE.NonEmpty Text
    , newDescr :: Text
    } deriving (Show)
