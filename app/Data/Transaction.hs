{-# LANGUAGE OverloadedStrings #-}
module Data.Transaction
( Transaction(..)
, makeTransaction
) where

-- Third-party imports
import Data.Text (Text, pack)
import Data.Text.Encoding (decodeUtf8)
import Data.Csv (Record)
import Data.Vector ((!))

-- Local imports
import Data.Tag (Tag(..))
import Data.Configurations ( getDateColumn, getDescriptionColumn
                           , getAmountColumn, getColumnIndex)
import Parsers.ConfigFile

data Transaction = Transaction
    { getTxnDate :: Text
    , getTxnDesc :: Text
    , getTxnAmt  :: Text
    , getTxnTags :: [Tag]
    } deriving (Show)

-- | Uses the given "InputFile" to parse the columns in the "Record" into a
--   valid "Transaction"
makeTransaction :: InputFile -> Record -> Either Text Transaction
makeTransaction inFileData record = do
    dateData <- getDataByIndex record dateCol
    descData <- getDataByIndex record descCol
    amtData  <- case getAmountData record amt of
                    Right "" -> Left $ "Blank amount found on this row:\n    "
                                <> pack (show record)
                    val      -> val
    tagData <- mapM (makeTagData record) tagColumns

    Right (Transaction dateData descData amtData tagData)
    where columnData = getInputColumns inFileData
          tagColumns = getInputTags inFileData
          dateCol = columnIndex (getDateColumn columnData)
          descCol = columnIndex (getDescriptionColumn columnData)
          amt     = getAmountColumn columnData

-- | Returns an approriate error if the column does not exist
getDataByIndex :: Record -> Int -> Either Text Text
getDataByIndex record n
    | n >= length record = Left . pack $ "Column " <> show n <>
                                         " requested, but only " <> show (length record) <>
                                         " columns available"
    | n < 0 = Left "Index but be >= 0"
    | otherwise = Right (decodeUtf8 $ record ! n)

-- | Handles the different types of possible Amount's, i.e. single vs.
--   multi-column
getAmountData :: Record -> AmountColumn -> Either Text Text
getAmountData record (AmountColumn col) = getDataByIndex record (columnIndex col)
getAmountData record (SplitAmount debit credit) =
    case getDataByIndex record (columnIndex debit) of
        -- If the debit column is blank, grab from credit column and add a
        -- negative
        Right "" -> fmap ("-" <>) (getDataByIndex record (columnIndex credit))
        -- Anything else gets passed on
        val -> val

-- | Takes a FileColumn and turns it into an Integer
columnIndex :: FileColumn -> Int
columnIndex = fromInteger . getColumnIndex

-- | Makes a single Tag from the inputs
makeTagData :: Record -> TagColumn -> Either Text Tag
makeTagData record (TagColumn tagName tagCol) =
    Tag tagName <$> getDataByIndex record tagCol'
    where tagCol' = columnIndex tagCol
